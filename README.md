# Homotopy type theory notes

A set of talks, notes and slides on Homotopy type theory. The source
code is available online at
<https://bitbucket.org/piyush-kurur/hott-notes/>.

## Getting a copy.

The notes here are maintained under the version controller git and
hosted on
[bitbucket](https://bitbucket.org/piyush-kurur/hott-notes/). You can
clone it using the following command.

```bash

git clone https://bitbucket.org/piyush-kurur/hott-notes/

```


## Licences and copyright

(C) Piyush P Kurur 2015

The source code, which includes, scripts, makefiles, agda samples etc
are released under the 2-clause BSD license. All documentation, notes,
installation guides, in particular non-executable files is released
under *Creative Commons Attribution-ShareAlike* license. For the exact
terms and conditions of these licences, see the respective files in
the sub-directory `licences`.

I welcome contributions from you. You need to do so by sending me an
appropriate pull request. However if you choose to contribute, it is
understood that

1. You agree to the release of the material contributed by you under
   the terms and conditions of the above license.

2. You agree to transfer the copyright to me, i.e. Piyush P Kurur. In
   return, I pledge as the primary author of this material, to make
   this and future versions under the same licensing conditions.

## Reference

An excellent introduction to the topic is the book available for free
at <http://homotopytypetheory.org/book/>.
