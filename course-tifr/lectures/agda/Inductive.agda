module Inductive where

data ℕ : Set where
  zero : ℕ
  succ : ℕ → ℕ

{-# BUILTIN NATURAL ℕ #-}

one   = succ zero
two   = succ one
three = succ two

_+_ : ℕ → ℕ → ℕ
0      + y = y
succ x + y = succ (x + y)

_*_ : ℕ → ℕ → ℕ
zero   * y = zero
succ x * y = (x * y) + y

data List (A : Set) : Set where
  nil  : List A
  _∷_ : A -> List A -> List A

-- Complete this as an exercise.
append : {A : Set} → List A → List A → List A
append nil      ys = ys
append (x ∷ xs) ys = x ∷ append xs ys

infixr 20 _∷_

mylist = one ∷ two ∷ three ∷ nil

-- A binary tree.
data Tree (A : Set) : Set where
     null : Tree A
     node : Tree A  -- left sub-tree
          → A       -- node label
          → Tree A  -- right sub-tree
          → Tree A

--      a                      b
--     / \                    / \
--   t₀   b         ==>      a  t₂
--       / \                / \
--      t₁ t₂             t₀  t₁
--
rotAntiClockWise   : {A : Set} → Tree A → Tree A
rotAntiClockWise ta = {!!}

inorder : {A : Set} → Tree A → List A
inorder ta = {!!}  -- Hole left as exercise.


-- Induction principles.


-- The induction principle for ℕ.
indℕ : {P : ℕ → Set}
     → P 0                            -- Base case
     → ( (n : ℕ) → P(n) → P(succ n) ) -- Induction step
     → (n : ℕ) → P(n)
indℕ base indStep zero     = base
indℕ base indStep (succ n) = indStep n (indℕ base indStep n)
