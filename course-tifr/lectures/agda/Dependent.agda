-- This module illustrates some dependent types.
module Dependent where

open import Data.Nat using (ℕ; _+_; _*_; zero;suc)
open import Relation.Binary.PropositionalEquality

-- First the vector type. Vectors for us a lists with the length of
-- the list encoded in the type. Thus for any type A, the type of
-- vectors of A of length 10 are distinct from vectors of length 42.

data Vector (A : Set) -- Vectors are parameterised over its element type.
     : ℕ              -- For a fixed type A, it is a type family over ℕ.
     → Set where

  -- The empty vector. Notice that
  nil  : Vector A 0

  -- Cons an element and the length increases by 1
  _∷_  : {n : ℕ}            -- This is an implicit argument.
       -> A                 -- first element
       -> Vector A n        -- rest of the vector
       -> Vector A (suc n)

infixr 20 _∷_

-- We can define the head function for vector.
head : {A : Set} → {n : ℕ} → Vector A (suc n) → A
head (x ∷ _) = x


append : {A : Set}{m n : ℕ}  -- implicitly quantify over all un-interesting stuff.
       → Vector A m           
       → Vector A n
       → Vector A (m + n)
append nil      ys = ys
append (x ∷ xs) ys = x ∷ append xs ys

--------- Red-black trees -------------------

-- A red black tree has nodes coloured either red or black, with
-- the following invariants.
--
-- 1. Every child of a red node is a black node.
--
-- 2. The number of black children from a node to
--    any leaf is the same.
--
-- Let us start by defining the colours of the node.

data Colour : Set where
  red   : Colour
  black : Colour


data Tree (A : Set) -- parameterised over the element type.
     : Colour       -- The colour of the root.
     → ℕ            -- The black depth.
     → Set where

  -- Empty tree is always considered black.
  null : Tree A black 0

  -- Constructor for a red node.
  nodeR  : {n : ℕ}
         → Tree A black n  -- left child should be black
         → A               -- node label
         → Tree A black n  -- right child should be black.
         → Tree A red  n   -- black height does not increase.

  -- Constructor for a black node.
  nodeB : {n : ℕ}
        → {cL cR : Colour}
        → Tree A cL n          -- left node, can be any colour.
        → A
        → Tree A cR n          -- can be any colour.
        → Tree A black (suc n) --

-- Predicates over naturals.
--
-- x ≤ y witness x is less than or equal to y.
data _≤_ : ℕ → ℕ → Set where
  -- Zero is less than all numbers.
  z≤m : {m : ℕ  }
      → 0 ≤ m
  -- If n is less than m then it is less than its successor.
  sn≤sm : {n m : ℕ}
      → n ≤ m
      → suc n ≤ suc m

reflexive    : (n : ℕ) → n ≤ n
reflexive n  = {!!}   -- Do the proof interactively.

transitivity : {a b c : ℕ}
             → a ≤ b
             → b ≤ c
             → a ≤ c
transitivity z≤m          bLEC         = z≤m
transitivity (sn≤sm aLEb) (sn≤sm bLEC) = sn≤sm (transitivity aLEb bLEC)

-- Strictly less than.
_<_ : ℕ → ℕ → Set
x < y = (suc x) ≤ y 

pred : (n : ℕ) → 0 < n → ℕ
pred zero    ()         -- This case will not arise.
pred (suc n) _ = n

----------------  Pi types and sigma types. ---------------------------

-- Sigma and pi types in agda.

-- Pi type is build in.
Pi : (A : Set)→ (P : A → Set) → Set
Pi A P = (x : A) → P x

-- Sigma type is a dependent pair.
data Σ {A : Set}(P : A → Set) : Set where
  _,_ : (a : A) → P a -> Σ P


_∧_   : (A B : Set) → Set
A ∧ B = Σ {A} (λ _ → B)
              --  λ x → e(x) means fun x → e(x).

-- This is the projection to the first component.
pr₁ : {A : Set}{P : A → Set}
    → Σ P
    → A
pr₁ (x , _) = x

-- This is the projection to the second component. The type of this
-- function is pretty interesting.
pr₂ : {A : Set}{P : A → Set}
    → (σ : Σ P)
    → P (pr₁ σ)
pr₂ (_ , y) = y


-- Formulation of the infinitely many primes theorem.
divisibleBy : ℕ → ℕ → Set
divisibleBy x y = Σ ExistsZ
  where ExistsZ   : ℕ → Set
        ExistsZ z = z * y ≡ x
          
4isDivisibleBy2 : divisibleBy 4 2
4isDivisibleBy2 = 2 , refl

prime : ℕ → Set
prime x = (y : ℕ) → 1 < y → divisibleBy x y → y ≡ x

primeLargerThan : ℕ → ℕ → Set
primeLargerThan x y = prime y ∧ (x ≤ y)

theorem : (x : ℕ) → Σ (primeLargerThan x)
theorem x = {!!}  -- Proof too big to fit in the margins of this talk.
                  -- i.o.w do it as exercise.
