module equality where

data _≡_ {A : Set} : A → A → Set where
  refl : {x : A} → x ≡ x


sym : {A : Set}
    → {x y : A}
    → (x ≡ y)
    → (y ≡ x)
sym refl = refl

-- Three proofs of transitivity.
trans trans1 trans2 : {A : Set}
                    → {x y z : A}
                    → (x ≡ y)
                    → (y ≡ z)
                    → (x ≡ z)

trans  refl refl = refl
trans1 refl yEz  = yEz
trans2 xEy  refl = xEy

-- Equality as paths.

-- Inverting paths is logically the symmetry of equality.
_⁻¹ : {A : Set}{a₀ a₁ : A}
    → a₀ ≡ a₁
    → a₁ ≡ a₀
p ⁻¹ = sym p

-- Concatnation of path is logically the transitivity of equality.
_∙_ : {A : Set}{a₀ a₁ a₂ : A}
    → a₀ ≡ a₁ → a₁ ≡ a₂
    → a₀ ≡ a₂
p ∙ q = trans p q
