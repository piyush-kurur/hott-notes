module Logic where


-- The type corresponding to a true proposition. The proof of this
-- statement should be trivial.
data ⊤ : Set where
  -- We have a single constructor which witnesses truth.
  obvious : ⊤

-- The type corresponding to a false propositon
data ⊥ : Set where
  -- No constructors so that ⊥ cannot be proved.

-- The AND of two statments.
data _∧_ (A B : Set)  : Set where
  -- The only way to construct a proof of A ∧ B is by pairing a a
  -- proof of A with a proof of and B.
  _,_ : A  -- Proof of A
      → B  -- Proof of B
      → A ∧ B    -- Proof of A ∧ B

-- The OR of two statements.
data _∨_ (A B : Set) : Set where
  -- There are two ways of constructing a proof of A ∨ B.
  inl : A →  A ∨ B   -- From a proof of A by left introduction
  inr : B →  A ∨ B   -- From a proof of B by right introduction

-- The not of statement A
¬_ : (A : Set) → Set
¬ A = A → ⊥  -- Given a proof of A one should be able to get a proof
             -- of ⊥.

-- The statement A ↔ B are equivalent.
_↔_ : (A B : Set) → Set
A ↔ B = (A → B) -- If
           ∧    -- and
        (B → A) -- only if


infixr 1 _∧_
infixr 1 _∨_
infixr 10 _,_
infixr 0 _↔_
infixr 2 ¬_


-- Let us prove the theorem A implies ¬ ¬ A
Theorem-Double-Negation : ∀ {A : Set} → A → ¬ ¬ A
Theorem-Double-Negation a negA = negA a

-- The converse is not provable in type theory. We can however make it
-- an axiom. However, note that there is no computational content for
-- this postulate.
postulate converse-Theorem-Double-Negation : {A : Set} → ¬ ¬ A → A

-- Given a proof of bottom I can prove whatever I want. This corresponds
-- to False ⇒ anything.
whatever : {A : Set} → ⊥ -> A
whatever ()

-- We can also prove weaker forms of some well known stuff from logic.
lemma : {P Q : Set}
      → ¬ P ∨ Q
      →   P → Q
lemma (inl f) p = whatever (f p)
lemma (inr g) p = g

-- We can also prove 3-quarter of De Morgan's laws.
deMorganI   : {A B : Set} → ¬ A ∧ ¬ B ↔ ¬ (A ∨ B)
-- For the second law, only one direction is provable.
deMorganII  : {A B : Set} → ¬ A ∨ ¬ B → ¬ (A ∧ B)

deMorganI {A} {B} = forward , backward
  where forward  : ¬ A ∧ ¬ B → ¬ (A ∨ B)
        backward : ¬ (A ∨ B) → ¬ A ∧ ¬ B
        forward (f , g) (inl a) = f a
        forward (f , g) (inr b) = g b
        backward nAOB  = (λ x → nAOB (inl x)) , (λ z → nAOB (inr z))

deMorganII (inl nA) (a , b) = nA a
deMorganII (inr nB) (a , b) = nB b
