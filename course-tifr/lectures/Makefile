LHS2TEX=lhs2TeX --poly
PANDOC=pandoc -f markdown+lhs -t beamer+lhs
PDFLATEX=pdflatex
GENERATED=tex aux log pdf nav out ptb snm vrb toc lhs.cont lhs

LECTURES=introduction
PDFS=$(addsuffix .pdf, ${LECTURES})

.DEFAULT_GOAL=all

%.lhs.cont : %.lhs.md
	${PANDOC} $< -o $@

%.lhs: %.lhs.cont
	echo '\documentclass{beamer}' > $@
	cat skin.lhs >> $@
	echo '%include ' $< >> $@
	echo '\end{document}' >> $@

%.tex: %.lhs %.lhs.cont symbols.fmt
	${LHS2TEX} $< > $@

%.pdf: %.tex
	pdflatex $<

.PHONY: all clean dual presentation

all: ${PDFS} symbols.fmt

clean:
	$(foreach lec, ${LECTURES},\
		rm -f $(addprefix ${lec}., ${GENERATED});)
	rm -f texput.log

dual:
	xrandr --output VGA1 --auto --output LVDS1 --auto --right-of VGA1

presentation:
	~/bin/pdfpc --notes=right introduction.pdf -t 16:00 -e 16:45 -d 45
