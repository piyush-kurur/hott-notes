# Talk source available online.

https://bitbucket.org/piyush-kurur/hott-notes/


\note{Set up the emacs window with the following files.}
\note[item]{Logic.agda}
\note[item]{Inductive.agda}
\note[item]{Dependent.agda}

# Type checking in programming languages.

- Programmer declares the types of each variable

> int x, y;
> string h = "hello";

- Compilers reject ill typed programs

> x + y; // this is fine
> x + h; // this is rejected

. . .

## What is the role  of types?

- Types are "specs" for your values.
- Compilers ensure that specs are not violated.

# Proof assistant

## Mathematics

. . .

A formal system for:

* expressing mathematical proofs
* machine checking proofs

. . .

## Programming

. . .

* specifying and proving program specs.
* synthesis programs matching specs.


# Executive summary: "Propositions as Types"

\pgfdeclarelayer{background}
\pgfsetlayers{background,main}
\begin{tikzpicture}[mindmap, concept color=red!50]
\visible<2->{
\node [concept]{Type Theory}
  %% programming
  child[grow=30, concept color=green!30!black, color=white]
          {node[concept](types){Types |Int|,|Bool| etc.}}
  child[grow=330, concept color=green!30!black, color=white]
          {node[concept](values){Values \newline |5 : Int|, |True : Bool|}}
  %% Proof assistant
  child[grow=150, concept color=orange]
          {node[concept](props){Propostions}}
  child[grow=210, concept color=orange]
          {node[concept](proofs){Proofs}};}
  \begin{pgfonlayer}{background}
     \node[fill=green!20,fit=(types)(values)](pl){Prog. Lang.};
     \node[fill=orange!20,fit=(props)(proofs)](pa){Proof assistant};
     \visible<3->{
     \draw [<->,concept connection]
            (types)
           edge [bend right]
           (props);
     }
     \visible<4->{
        \draw [<->,concept connection] (values) edge [bend left](proofs);
     }
  \end{pgfonlayer}
\end{tikzpicture}

# Executive summary: Homotopical content in type theory.

For a type $A$ and $x, y : A$, there is a type $x == y$.

. . .

1. $A$ can be viewd as a topolotical space

2. $x:A$ as points of $A$.

3. $p : x \equiv y$ as path from $x$ to $y$.

4. Homotopies, covers, fibrations, truncations etc have natural type theoretic
   interpretations.

# Understing "Propositions as types": A toy programming language.

## Types.

> types  =  Int     |  Bool | ...  (Basic types)
>        |  A -> B                 (functions from A to B)
>

. . .

## Terms.

> terms  =  2    | True ...         (constants)
>        |  x                       (variables)
>        |  f e                     (function application)
>        |  fun  (x : A) -> e(x)    (function abstraction)


# Type checking rules

## Rule for function application (Elimination)

>  f    : A -> B
>  e    : A
>  ==============
>  f e  : B

. . .

## Rule for function abstraction (Introduction)



>
> x     :  A
> ==========        ==>   ===============
> e(x)  :  B              fun (x : A) -> e (x) : A -> B
>
>


# Rule for inference for $A \times B$.

## Introduction rule

> x      :  A
> y      :  B
> ================
> (x,y)  :  A >< B

## Elimination rule.

> u      :  A >< B
> ================
> pr1 u  :  A


# Propositional logic and type theory.

> A  type     <->  Proposition
> A  ->  B    <->  A implies B
> A  ><  B    <->  A and B
> A  +   B    <->  A or  B
> Top         <->  true
> Bot         <->  false
> A  ->  Bot  <->  ~ A


\note{Time to show the following:}
\note[item]{Logic.agda}
\note[item]{Inductive.agda}

# Predicates Logic and type families.

* $P(x)$ : $x$ is a prime number.
* $P(2)$, $P(3)$ are true but not $P(42)$ for example
* $\forall x\in \mathbb{N}\; \exists y \in \mathbb{N}\;\;
  x \leq y \wedge P(y)$

. . .

## Type family.

- For $a : A$, $P(a)$ is a type.

- Proof assistant: Predicates over $A$.

- Programming: More complicated "specs" of values
  List of size n, Integers modulo n.

\note{Time to show: Dependent.agda}
\note[item]{Show till PiTypes Sigmatypes}

# Capturing $\forall x P(x)$: The $\Pi$-type

$A$ is a type and $P$ a type family on $A$.

## Proofs of $\forall x : A\; P(x)$

- Functions $f$ that given $x : A$ produce $f(x) : P(x)$

- Crucial property: the type of $f(x)$ depends on $x$.

- The type of such functions is $\Pi_A P$

- Generalisation of $A \to B$

# Capturing $\exists x : A\; P(x)$ : The $\Sigma$-type


$A$ is a type and $P$ a type family on $A$.

## Proof of $\exists x : P(x)$.

- A witness $w$ for which $P(w)$ holds

- A proof that $P(w)$

- Elements are pairs $(x, y)$ where $y : P(x)$

- The type of such pairs is $\Sigma_A P$

- Generalisation of $A \times B$.

# Predicate logic and type theory.

Let $\mathcal{U}$ be the type of all types.

> P  :  A  ->  Set   <->  Predicate P on A
> (x : A)  ->  P x   <->  forall x : A P(x)
> Sigma_A      P(x)  <->  exists x : A P(x)

\note{Time to show: Dependent.agda}

# Homotopy theory.

Studying spaces (topological) under deformations.

. . .

## Basic notions

- Paths in a space

- Homotopies, i.e. deformations of paths.

- Higher homotopies.

# Homotopic paths:

\begin{tikzpicture}
\tikzset{every node/.style=
	{shape=circle,minimum height=0pt, inner sep=2pt, fill=black!80}
}
\path  (0,0) node [label= left:$x$] (x){}
    -- (5,0) node [label= right:$y$] (y){};

\begin{scope}
  \tikzset{very thick, color=black,
  decoration={markings, mark=at position 0.5 with {\arrow{>}}}}
  \draw[postaction={decorate}, fill=blue!30]
  (x.east) .. controls (1,2) and (2,2).. (y.west)
	node[fill=none, midway, above]{$p$};
  \draw[postaction={decorate}, fill=blue!30 ]
	(x.east)
  .. controls (3,-2) and (4,-2)
  .. (y.west) node[fill=none, midway, below]{$q$};
  \pause
  \draw[dashed,postaction={decorate}] (x.east)
  .. controls (1,1.5) and (2, 1.45) .. (y.west);
  \pause
  \draw[dashed,postaction={decorate}] (x.east)
  .. controls (1,0.5) and (2,1) .. (y.west);
  \pause
  \draw[dashed,postaction={decorate}] (x.east)
  .. controls (1,0.1) and (2,-0.1) .. (y.west);
  \pause
  \draw[dashed,postaction={decorate}] (x.east)
  .. controls (3,-1) and (4,-0.5) .. (y.west);
  \pause
  \draw[dashed,postaction={decorate}] (x.east)
  .. controls (3,-1.45) and (4,-1.5) .. (y.west);
\end{scope}
\end{tikzpicture}

# Paths that are not homotopic.

\begin{tikzpicture}
\tikzset{every node/.style=
	{shape=circle,minimum height=0pt, inner sep=2pt, fill=black!80}
}
\path  (0,0)   node [label= left:$x$] (x){}
    -- (5,0) node [label= right:$y$] (y){};

\begin{scope}
  \tikzset{very thick, color=black,
  decoration={markings, mark=at position 0.5 with {\arrow{>}}}}
  \draw[postaction={decorate}, fill=blue!30]
  (x.east) .. controls (1,2) and (2,2).. (y.west)
	node[fill=none, midway, above]{$p$};
  \draw[postaction={decorate}, fill=blue!30 ]
	(x.east)
  .. controls (3,-2) and (4,-2)
  .. (y.west) node[fill=none, midway, below]{$q$};

  \node[shape=circle, radius=1, fill=yellow!30] at (2.5,0) {Hole};
\pause
\draw[postaction={decorate}]
  (0.5,0) .. controls (1,2) and (2,-1) .. cycle;
\pause
\draw[postaction={decorate}]
   (1.5,0) .. controls (2.5,1.5) and (4,0.5)
           .. (4,0) .. controls (4,-1) and (2.5,-1) .. cycle;

\end{scope}

\end{tikzpicture}

# The fundamental group $\pi_1(A,x)$

Consider loops up to homotopic identification.

. . .

1. Elements are loops based on $x$.

2. Identity is the trivial loop

3. Inverse is the loop that goes in the reverse direction.

4. Multiplication is path concatenation

# Some examples

## $\pi_1$ is trivial for.

* Real plane $\mathbb{R}^2$

* 2-sphere $\mathbb{S}^2$ and punctured 2-sphere

. . .

## Nontrivial

* 1-sphere $\mathbb{S}^1$

* Punctured real plane $\mathbb{R}^2$,

* $\mathbb{S}^1 \vee \mathbb{S}^1$.

* Torus etc

# Covering spaces.

\begin{columns}
\begin{column}{0.5\textwidth}
\pgfdeclarelayer{background}
\pgfsetlayers{background,main}
\begin{tikzpicture}
\tikzset{every node/.style=
	{shape=circle,minimum height=0pt, inner sep=2pt}
}
\tdplotsetmaincoords{70}{10}
\draw [->,smooth, fill=red!10, domain=0:360] plot (1.5 * sin \x, -2, 1.5 * cos \x);
\node [right] at (0, -2, 0) {$\mathbb{S}^1$};
\begin{pgfonlayer}{background}
  \path[fill=red!10] (2,-2,2) .. controls (3,-2,1) and (1,-2,-1) .. (2,-2,-2)
  -- (-2,-2,-2) -- (-2,-2,2) -- cycle;
\end{pgfonlayer}
\draw [->] (-2,-2,2) -- (3,-2,2);
\draw [->] (-2,-2,2) -- (-2,-2,-3);
\node [below] at (-1,-2,2) {$x$};
\node [left]  at (-2,-2,0) {$y$};
\pause
\draw [->] (-2,-2,2) -- (-2,3,2);
\node [left] at (-2,-1,2) {$z$};
\draw [->, blue, smooth, domain=0:1080]
	plot (1.5 * sin \x, \x/360, 1.5 * cos \x);
\draw [dashed, blue, smooth, domain=1080:1125]
	plot (1.5 * sin \x, \x/360, 1.5 * cos \x);
\draw [dashed, blue, smooth, domain=-45:0]
    plot (1.5 * sin \x, \x/360, 1.5 * cos \x);
\pause
\node[fill=black] at (1.5,-2,0) (x) {};
\node[right] at (x.east) {$x$};
\pause
\draw[<-, dotted]
   (x)
   -- (1.5,0.25,0) node [fill=black!80] (x0) {}
   -- (1.5,1.25,0) node [fill=black!80] (x1) {}
   -- (1.5,2.25,0) node [fill=black!80] (x2) {}
   -- (1.5,3.5,0)  node (fiber){};
\node[right] at (x0.east) {$x_0$};
\node[right] at (x1.east) {$x_1$};
\node[right] at (x2.east) {$x_2$};
\pause
\node[above] at (fiber.north) (fibt){$\mathrm{Fiber}(x)$};
\end{tikzpicture}
\end{column}

\pause

\begin{column}{0.5\textwidth}
\only<6>{
\begin{itemize}
\item Type family $F : A \to \mathcal{U}$ is a fibration.
\item $\Sigma_A F$ is the cover space,
\item $\mathrm{pr}_1 : \Sigma_A F \to A$ is the covering map.
\end{itemize}
}
\end{column}

\end{columns}

# Homotopic interpretations of types.

> A type              <->  topological space
> x  :   A            <->  point x in A
> x  ==  y            <->  path from x to y
> symmetry of ==      <->  path inverse
> transitivity of ==  <->  path concatenation
> alpha  :   p == q   <->  homotopy from p to q.
> P  :   A -> Set     <->  Fibration
> Sigma_A P           <->  Covering space
> Pi_A P              <->  Sections of P

# Loop spaces and pointed types.

## Pointer types

Type $A$ together with a base point $a : A$.

. . .

* Let $\mathcal{U}$ be the type of all types.

* $P : \mathcal{U} \to \mathcal{U}$ defined by $P (A) = A$.

* $\mathcal{U}_\bullet = \Sigma_{A : \mathcal{U}} P$.

. . .

## Loop space.

. . .

* $\Omega : \mathcal{U}_\bullet \to \mathcal{U}_\bullet$

* $\Omega(A, a) = (a \equiv a, \mathrm{refl}_a)$.

. . .

* $\Omega^{i+1} = \Omega(\Omega^i(A,a))$.


# Foundations for mathematics.

1. Higher inductive types (HITs),

2. Voevodsky's Univalence Axiom,

3. H-levels and Recovering set mathematics.

# Implications

. . .

## For mathematics

. . .

* New foundation, better for machine checking

* Massive proof projects like classification of finite simple
   groups.

* Brutal proof methods for routine but tedious stuff.

. . .

## For programming

. . .

* For programming new types of computations
* Programming languages based on exotic types, in particular quotients.
