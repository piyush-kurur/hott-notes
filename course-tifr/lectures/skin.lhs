\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{bbold}
\usepackage{tikz}
\usepackage{pgfpages}
\setbeameroption{show notes on second screen=right}
\usetikzlibrary{mindmap}
\usetikzlibrary{backgrounds,shapes,shapes.geometric,arrows,decorations.markings}
\usetikzlibrary{fit}
\usepackage{tikz-3dplot}

\usepackage{longtable}
%include symbols.fmt
\title{Types, proofs, programs and homotopy}

\author{
  Piyush P. Kurur\\
  Dept Of Computer Science and Eng\\
  Indian Institute Of Technology\\
  Kanpur, UP 208016, India\\
  {\tt ppk@@cse.iitk.ac.in}\\
}


\newtheorem{proposition}[theorem]{Proposition}

\begin{document}
\begin{frame}
\maketitle
\end{frame}

